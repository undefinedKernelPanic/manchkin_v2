require "manchkin2/player_modules/equip"

module Manchkin2
  module PlayerModules
    class PlayerCharacteristics

      include Equip

      def initialize(sex)
        @sex = sex
        @klass = :human
        @level = 1
        # @power = @level
        # @hand = []
        # @carried = []
        # @equipped = []
      end
    end
  end
end