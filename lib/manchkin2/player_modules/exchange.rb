module Manchkin2
  module PlayerModules
    module ExchangeCards

      def self.included(base = nil, &block)
        base.instance_eval do

          attr_accessor :exchange_dialog, :state_before_exchange_dialog


          # define_method :exchange_dialog do # возвращает диалог обмена
          #   unless instance_variable_defined?("@exchange_dialog")
          #     instance_variable_set "@exchange_dialog", nil
          #   end
          #   instance_variable_get "@exchange_dialog"
          # end
          #
          # define_method :exchange_dialog= do |exchange_dialog|
          #   instance_variable_set "@exchange_dialog", exchange_dialog
          # end
        end
      end

      # def send_exchange_request(initiator, receiver)

      end

      def give_cards_to(player) # рубашкой вверх
        dialog_creation_wrap player, :open
      end

      def offer_exchange_to(player) # все карты рубашкой вверх
        dialog_creation_wrap player, :open
      end

      def offer_choose_card_to(player) # карты рубашкой вниз
        dialog_creation_wrap player, :hide, :cant_change
      end

      private

      def dialog_creation_wrap(player, mode, changeable = nil)
        dlg = start_dialog player, mode: mode, can_change: changeable.nil?
        @exchange_dialog = dlg
        player.exchange_dialog = dlg
      end

      def start_dialog(receiver_player, args = {}) # possible args: mode: hide/open
        dlg = ExchangeDialog.new self, receiver_player, args

        # @state_before_exchange_dialog = @state
        # change_state ::Manchkin2::PlayerOfferDialog.instance

        # receiver_player.state_before_exchange_dialog = receiver_player.state
        # receiver_player.change_state ::Manchkin2::PlayerIncomingDialog.instance
        dlg
      end

    # end
  end
end