module Manchkin2
  module PlayerModules
    module Equip

      def self.included(base = nil, &block)
        base.instance_eval do
          define_method :hand do
            unless instance_variable_defined?("@hand")
              instance_variable_set "@hand", []
            end
            instance_variable_get "@hand"
          end

          define_method :carried do
            unless instance_variable_defined?("@carried")
              instance_variable_set "@carried", []
            end
            instance_variable_get "@carried"
          end

          define_method :equipped do
            unless instance_variable_defined?("@equipped")
              instance_variable_set "@equipped", []
            end
            instance_variable_get "@equipped"
          end
        end
      end

      def power
        @level + gear_bonuses + other_bonuses
      end

      private

      def other_bonuses
        0
      end

      def gear_bonuses
        head_bonus + chest_bonus + boots_bonus + weapon_bonus
      end

      def add_to_hand(card, how_received = :hide)
        @hand.push PlayerCard.new card, how_received
      end

      # def equip!(player_card_id)
      #   @equipped.push @carried.delete(find_by_id player_card_id)
      # end

      def put_on_desk!(player_card_id)
        @carried.push @hand.delete(find_by_id player_card_id)
      end

      def find_by_id(player_card_id)
        @carried.find { |card| card.id == player_card_id}
      end



    end

    class PlayerCard
      def initialize(card, how_received) # :hide, :open
        @id = IdGenerator.generate_id
        @card = card
        @how_received = how_received
      end

      def received_open?
        @how_received == :open
      end

      def received_hide?
        @how_received == :hide
      end
    end
  end
end