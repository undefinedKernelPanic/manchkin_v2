module Manchkin2
  module Support
    module StateTransitionCallbacks

      def self.included(base = nil, &block)
        base.instance_eval do
          define_method :transition_callbacks do # возвращает все коллбеки стейта
            unless instance_variable_defined?("@transition_callbacks")
              instance_variable_set "@transition_callbacks", []
            end
            instance_variable_get "@transition_callbacks"
          end
        end
      end

      def before_transition(args = {}, &block) # to & from, permanent
        callback = StateCallback.new(IdGenerator.generate_id, :before, args[:from], args[:to], args[:permanent] || false, block)
        transition_callbacks.push callback

        callback.id
      end

      def after_transition(args = {}, &block) # to & from, permanent
        callback = StateCallback.new(IdGenerator.generate_id, :after, args[:from], args[:to], args[:permanent] || false, block)
        transition_callbacks.push callback

        callback.id
      end

      def change_state(new_state)
        old_state = @state

        run_actions :before, old_state, new_state
        @state = new_state
        run_actions :after, old_state, new_state
      end

      def remove_state_callback(callback_id)
        transition_callbacks.delete_if { |callback| puts callback.id == callback_id; callback.id == callback_id }
        p 'removed'
      end

      private

      def run_actions(when_start, from, to)
        transition_callbacks
          .select do |callback|
            callback.when_action == when_start &&
            (callback.from.nil? || callback.from == from) &&
            (callback.to.nil? || callback.to == to)
          end
          .each do |callback|
            callback.action.call self
            remove_state_callback callback.id unless callback.permanent?
          end
      end

    end

    class StateCallback

      attr_reader :id, :when_action, :from, :to, :permanent, :action

      def initialize(id, when_action, from, to, permanent, action)
        @id = id
        @when_action = when_action
        @from = from
        @to = to
        @permanent = permanent
        @action = action
      end

      def permanent?
        @permanent
      end
    end
  end
end