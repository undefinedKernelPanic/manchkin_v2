class IdGenerator
  def self.generate_id(count = 16)
    (('a'..'z').to_a + (0..9).to_a).shuffle[0, count].join
  end
end