#TODO add remove callback by id globally


module Manchkin2

  module Support

    module StateDependentActions

      # if confirmable action, def method
      # def start_confirmable_action(action_result, after_actions)
      #   @action_result = action_result
      #   @state_before_waiting = @state
      #   @after_actions = after_actions
      #   change_state Manchkin2::PlayerWaitingConfirmation.instance
      # end
      # attr :required_actions

      def change_state(state) # replaced by StateTransitionCallbacks
        @state = state
      end

      def can_perform_action?(method)
        @state.class.instance_methods(false).include? method
      end

      def add_callback(mod, method_name, is_permanent = false, &callback)

        inst_var = send :"#{method_name}_action"
        inst_var.send :"add_#{mod}_action", is_permanent, callback
      end

      # def add_required_action(name) # name, permanent
      #   required_actions.push name
      # end

      def self.included(base = nil, &block)

        base.instance_eval do

          define_method :required_actions do
            unless instance_variable_defined?("@required_actions")
              instance_variable_set "@required_actions", []
            end
            instance_variable_get "@required_actions"
          end


          def define_methods(&block)

            proxy = Proxy.new

            if block_given?
              proxy.instance_eval &block
            end

            proxy.methods.each do |hash|

              if hash[:confirmable] && !respond_to?(:start_confirmable_action)
                define_method :start_confirmable_action do |action_result, after_actions|
                  @action_result = action_result # результат действия
                  @state_need_to_return_after_transition_from_wait_confirm = @state
                  @after_actions = after_actions # коллбеки полсле экшена
                  @action_when_confirm = nil # что сделать когда игрок подтвердил
                  change_state hash[:state_while_confirm]
                end
              end

              define_method :"#{hash[:method_name]}_action" do
                if hash[:type] == :state_depended
                  raise "экшен #{hash[:method_name].to_s} не поддерживает коллбеки"
                end
                unless instance_variable_defined?("@#{hash[:method_name].to_s}_action")
                  e = Class.new BaseAction

                  action = e.new self, hash[:method_name], hash[:confirmable]

                  e.class_eval do
                    def run_instead_action(args)
                      @action_result = @instead.nil? ? @object.instance_variable_get("@state").public_send(@method_name.to_sym, *args) : @instead.call(@object)
                      unless @instead&.permanent? # все норм, безопасный оператор &.
                      @instead = nil
                      end
                    end
                  end

                  self.instance_variable_set "@#{hash[:method_name].to_s}_action", action
                end
                return instance_variable_get("@#{hash[:method_name]}_action")
              end

              define_method hash[:method_name] do |*args|
                if can_perform_action? __method__
                  if hash[:type] == :state_depended # если просто зависит от стейта, то сразу вызвать у стейта метод
                    self.state.public_send hash[:method_name], self, *args
                    return
                  end
                  self.send(:"#{hash[:method_name]}_action").call self, *args
                else
                  puts 'нельзя это сделать'
                end

              end
            end

          end

        end

      end


    end


    class BaseAction
      class Action

        attr_reader :id

        def initialize(action, is_permanent)
          @id = IdGenerator.generate_id
          @action = action
          @is_permanent = is_permanent
        end

        def call(object = nil, action_result = nil)
          @action.call object, action_result
        end

        def permanent?
          @is_permanent
        end
      end

      def initialize(object, method_name, is_confirmable = false)
        @method_name = method_name
        @object = object
        @before = []
        @instead = nil
        @after = []
        @action_result = nil
        @is_confirmable = is_confirmable
      end

      def confirmable?
        @is_confirmable
      end

      def call(*args)
        run_before_actions
        run_instead_action args
        if confirmable?
          @object.start_confirmable_action @action_result, method(:run_after_actions)
        else
          run_after_actions
        end
      end

      def add_before_action(is_permanent = false, action)
        # p 'PlayerAction:before'
        new_action = Action.new(action, is_permanent)
        @before.push new_action
        new_action.id

      end

      def add_after_action(is_permanent = false, action)
        new_action = Action.new(action, is_permanent)
        @after.push new_action
        new_action.id
      end

      def add_instead_action(is_permanent = false, action)
        new_action = Action.new(action, is_permanent)
        @instead = new_action
        new_action.id
      end

      def remove_action(action_id)
        @after.delete_if { |action| action.id == action_id }
        @before.delete_if { |action| action.id == action_id }
        @instead == nil if @instead.id == action_id
      end

      private

      def run_before_actions
        @before.each do |action|
          action.call @object
          unless action&.permanent?
          @before.delete action
          end
        end
      end

      def run_after_actions(result = @action_result)
        @after.each do |action|
          action.call @object, result
          unless action&.permanent?
          @after.delete action
          end
        end
        # @after = []
      end

      def run_instead_action(args)
        @instead = nil
      end
    end

    # ---------------------------------------------------------------------------

    class Proxy

      attr_reader :methods

      def initialize
        @methods = []
      end

      def def_method(args = {}) # or :both or :callbackable or :state_depended
        raise 'need specify method name' if args[:name].nil?
        raise 'need state while confirm if method is confirmable' if args[:confirmable] && args[:state_while_confirm].nil?
        p = { type: args[:type] || :state_depended, method_name: args[:name], confirmable: args[:confirmable] || false,
              state_while_confirm: args[:state_while_confirm] || nil }
        @methods.push p
      end
    end

  end
end