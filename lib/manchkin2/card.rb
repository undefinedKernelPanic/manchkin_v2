module Manchkin2
  class Card
    # attr :name
    attr_reader :subtype, :type, :name, :id
    # def initialize
    #   @subtype = self.class.to_s.to_sym.downcase
    # end
    def subtype
      self.class.to_s.sub('Manchkin::', '').to_sym.downcase
    end

  end

  class Treasure < Card
    # def initialize
    #   @type = :treasure
    #   super()
    # end

    def card_type
      :treasure
    end
  end

  class Door < Card
    # def initialize
    #   @type = :door
    #   super()
    # end

    def card_type
      :door
    end
  end

  class Monster < Door
    def initialize(name, level)
      @level = level
      @name = name
      # super()
    end
  end

  class Curse < Door

  end

  class Modificator < Door

  end

  class Klass < Door

  end

  class Race < Door

  end

  class Gear < Treasure

  end

  class Potion < Treasure

  end

  class LevelUp < Treasure

  end
end