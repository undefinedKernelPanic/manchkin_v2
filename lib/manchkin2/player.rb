require "manchkin2/support/state_dependent_actions"
require "manchkin2/support/state_transition_callbacks"
require "manchkin2/states/player_states"
require "manchkin2/player_modules/player_characteristics"


module Manchkin2
  class Player
    include Manchkin2::Support::StateDependentActions
    include Manchkin2::Support::StateTransitionCallbacks

    attr_reader :name, :sex, :game, :id, :state

    define_methods do
      # def_method :callbackable, :open_door, true
      def_method type: :callbackable, name: :open_door,
                 # confirmable: true, state_while_confirm: Manchkin2::PlayerWaitingConfirmation.instance,
                 delegate_to: @state do |player|

      end

      def_method name: :confirm
    end

    def initialize(uniq_name, sex)
      @id  = IdGenerator.generate_id
      @game = nil
      @name = uniq_name
      @characteristics = PlayerCharacteristics.new sex
      # @sex = sex
      # @klass = :human
      # @level = 1
      # @power = 1
      # @hand = []
      # @carried = []
      # @equipped = []

      @state = ::Manchkin2::PlayerReady.instance

      add_callback :before, :open_door, true do |pl|
        puts 'player trying to open door'
      end

      add_callback :after, :open_door, true do |pl, result|
        puts "action result     " + result.to_s
        puts 'after open door'
      end

      before_transition to: ::Manchkin2::PlayerOpenDoor.instance do |player|
        p player.name
        p 'before transition on ::Manchkin2::PlayerOpenDoor.instance'
      end

      after_transition from: ::Manchkin2::PlayerReady.instance, to: ::Manchkin2::PlayerOpenDoor.instance do |player|
        p player.state
        p 'after transition on ::Manchkin2::PlayerOpenDoor.instance'
      end

      # @open_door_action = Manchkin2::PlayerActionOpenDoor.new self
    end


    # ----------------callbackable actions that depend on state--------------

    # def open_door
    #
    #   if can_perform_action? __method__
    #     @open_door_action.call self
    #   end
    # end

    # ----------------//callbackable actions that depend on state--------------
    # def add_action(action_name)
    #
    # end

    private

    # def can_perform_action?(method)
    #   @state.class.instance_methods(false).include? method
    # end

    # def change_state(state) # replaced by StateTransitionCallbacks
    #   @state = state
    # end

    def attach_to_game(game)
      @game = game
    end

    def add_cards(cards)
      if cards.is_a? Array
        @hand += cards
      else
        @hand.push cards
      end
    end

  end
end