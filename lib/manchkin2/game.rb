require "manchkin2/support/state_dependent_actions"
require "manchkin2/states/game_states"

module Manchkin2
  class Game

    include Manchkin2::Support::StateDependentActions

    define_methods do
      def_method type: :s, name: :add_player
      def_method type: :s, name: :open_door
      def_method type: :s, name: :start_game
      def_method type: :s, name: :throw_dice
      # def_method type: :state_depended, name: :logger

      # def_method :w, :add_player
      # def_method :w, :open_door
      # def_method :w, :start_game
    end

    attr_reader :players, :logger, :current_player, :battle, :state, :deck

    def initialize(deck)
      @logger = Manchkin2::Logger.new
      @id = IdGenerator.generate_id
      @deck = deck
      @players = []
      @current_player = nil
      @battle = nil
      @state = ::Manchkin2::GameNew.instance


      # y = add_callback :before, :add_player do |object|
      #   p object.class
      #   p 'before'
      # end
      # puts y
      #
      # @add_player_action.add_after_action true do |object, result|
      #   p 'after'
      #   p result
      #   # object.instance_variable_get("@add_player_action").remove_action y
      # end
      #
      # @add_player_action.add_instead_action true do |object|
      #   p 'instead'
      #   4
      # end
    end

    # ----------------callbackable actions that depend on state--------------





    # ----------------//callbackable actions that depend on state--------------


    def player(player_name) # find player by name
      @players.find { |player| player.name == player_name } || raise("player #{player_name} not found")
      # player = @players.find { |player| player.name == player_name }
      # raise "player #{player_name} not found" if player.nil?
      # player
    end

    def other_players # all, except current player
      @players.select { |player| player != @current_player }
    end

    private

    # def change_state(state)
    #   @state = state
    # end

    # def can_perform_action?(method)
    #   @state.class.instance_methods(false).include? method
    # end

  end
end