module Manchkin2
  class Deck

    attr_reader :doors, :treasures, :cards

    def initialize(cards)
      @cards = cards
      @doors, @treasures = cards.partition { |card| card.is_a? Door }
      @doors_discard = []
      @treasures_discard = []
    end

    def shuffle!
      @doors.shuffle!
      @treasures.shuffle!
    end

    def draw_start_hand
      [
          draw_card,
          draw_card,
          draw_card(:treasure),
          draw_card(:treasure)
      ]
    end

    def draw_card(*args)
      deck = args.select { |i| i == :door || i == :treasure }.first || :door
      case deck
      when :door
        return @doors.shift
      when :treasure
        return @treasures.shift
      end
    end

  end

end