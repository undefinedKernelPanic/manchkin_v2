module Manchkin2
  class ExchangeDialog
    def initialize(player_initiator, player_receiver, args)
      @id = IdGenerator.generate_id
      @members = [
          DialogMember.new(player_initiator, :initiator),
          DialogMember.new(player_receiver, :receiver)
      ]
      # @player_initializer = player_initializer
      # @player_receiver = player_receiver

      player_initiator.change_state ::Manchkin2::PlayerOfferDialog.instance
      player_receiver.change_state ::Manchkin2::PlayerIncomingDialog.instance
      @mode = args[:mode]
      @can_change = args[:can_change]

    end

    def add_card(player, card)
      get_member_by_player(player).add_card card
    end

    def remove_card(player, card)
      get_member_by_player(player).remove_card card
    end

    def initiator
      @members.find { |member| member.role == :initiator }
    end

    def receiver
      @members.find { |member| member.role == :receiver }
    end

    def get_member_by_player(player)
      @members.find { |member| member.player == player }
    end

    def terminate
      @members.each do |member|
        member.player.exchange_dialog = nil
        member.player.change_state member.state_before_exchange
      end

      # @player_initializer.exchange_dialog = nil
      # @player_receiver.exchange_dialog = nil
      # @player_initializer.change_state @player_initializer.state_before_exchange_dialog
      # @player_receiver.change_state @player_receiver.state_before_exchange_dialog
    end

    def accept_dialog
      @members.each do |member|
        member.player.change_state ::Manchkin2::PlayerExchangeDialog.instance
      end
    end
  end

  class DialogMember

    attr_reader :player, :role, :state_before_exchange

    def initialize(player, role)
      @state_before_exchange = player.state
      @player = player
      @role = role
      @added_cards = []
    end

    def add_card(card)
      @added_cards.push card
    end

    def remove_card(card)
      @added_cards.delete card
    end
  end
end