Manchkin::Deck.define :classic do
  card :monster do
    level 10
    name '3872 орка'
    treasures 3
    monster_actions -> (monster_character) do
      monster_character.have_bonus_against :dwarf, 4
    end
    bad_stuff -> (player) do
      # только для игроков в состоянии PlayerSufferBadStuff
      player.add_action :throw_dice # пихает в массив ghost методов игрока
      # после выполения удаляется
      player.add_callback :before, :throw_dice do
        p 'before throw'
      end
      player.add_callback :after, :throw_dice do |player1, result_of_throw_dice|
        # кудато надо сохранять результат действия
        if (1..2).include? result_of_throw_dice
          player1.death!
        else
          player1.loose_level result_of_throw_dice
        end
        true

      end
    end

  end

end