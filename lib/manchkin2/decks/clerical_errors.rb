Manchkin2.define_deck :clerical_errors, :unnatural_exe do
  card :monster do
    p 'monster form cleric'
  end

  card :gear do
    p 'gear form cleric'
  end
end
