Manchkin2.define_deck :unnatural_exe, :classic do
  card :monster do
    p 'monster form unn'
  end

  card :gear do
    p 'gear form unn'
  end
end
