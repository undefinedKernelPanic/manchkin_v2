# require "manchkin2/support/state_dependent_actions"
# require "manchkin2/states/battle_states"
# require "manchkin2/battle_chars/battle_characters"


# module Manchkin2

#   class Battle
#     include Manchkin2::Support::StateDependentActions

#     attr_reader :state

#     define_methods do
#       def_method type: :callbackable, name: :add_character
#       def_method type: :callbackable, name: :character

#       # def_method :d, :start, true
#       # start_game
#     end


#     def initialize(game)
#       @id = IdGenerator.generate_id
#       @players = []
#       @monsters = []

#       # @players.push ::Manchkin2::BattleChars::PlayerCharacter.new player, :owner, self
#       # @monsters.push ::Manchkin2::BattleChars::MonsterCharacter.new monster_card, self


#       @state = ::Manchkin2::BattleInProgress.instance
#       #
#       # y = add_callback :before, :start, true do |object|
#       #   p object.class
#       #   p 'before ept'
#       # end
#       # puts y
#       #
#       # add_callback :after, :start do |object, res|
#       #   p res
#       #   p 'after ept'
#       # end
#       #
#       # add_callback :instead, :start do |object|
#       #   p 'omg'
#       #   p object
#       #   3
#       # end
#     end

#     # def add_character(args = {}) # monster: monster_card, player: player, player_role: (:owner, :helper)
#     #   if args.has_key? :monster
#     #     add_monster args[:monster]
#     #   else
#     #     if args.has_key?(:player) && args.has_key?(:player_role)
#     #       add_player args[:player], args[:player_role]
#     #     end
#     #   end
#     # end

#     private

#     def add_player(player, role)
#       @players.push ::Manchkin2::BattleChars::PlayerCharacter.new player, role, self
#     end

#     def add_monster(monster_card)
#       @monsters.push ::Manchkin2::BattleChars::MonsterCharacter.new monster_card, self
#     end


#   end

#   # class BattleCharacter
#   #   attr_reader :power
#   # end
#   #
#   # class BattleMonster < BattleCharacter
#   #
#   #   CONST = {
#   #     :dwarf => :klass
#   #   }
#   #
#   #   attr_reader :treasures_count, :state, :monster
#   #   attr_accessor :bonuses
#   #
#   #   def initialize(card_monster, game)
#   #     @monster = card_monster
#   #     @game_id = game.id
#   #     @attached_cards = []
#   #     @bonuses = []
#   #     @power = 0
#   #     # @bonus_power = 0
#   #     @level = card_monster.level
#   #     @treasures_count = @monster.treasures
#   #
#   #     p @monster
#   #     p 'BattleMonster initialize'
#   #     p @monster.respond_to? :monster_actions
#   #     @monster.monster_actions.call(self) if @monster.respond_to? :monster_actions
#   #
#   #     # @treasures_count = @monster.treasures
#   #     @state = :fit_as_fiddle # может быть :defeated, :killed
#   #
#   #     recalculate_monster_attrs!
#   #   end
#   #
#   #   def game
#   #     ObjectSpace._id2ref @game_id
#   #   end
#   #
#   #   def attach_card(card)
#   #     @attached_cards.push card
#   #     recalculate_monster_attrs!
#   #   end
#   #
#   #   def defeated?
#   #     state != :fit_as_fiddle
#   #   end
#   #
#   #   def have_bonus_against(against_who, count)
#   #     # type = klass
#   #     p 'BattleMonster have_bonus_against'
#   #     # p CONST
#   #     h = { type: CONST[against_who], how: against_who, count: count }
#   #     @bonuses.push h
#   #   end
#   #
#   #   def bonus_power
#   #     @bonuses.inject(0) do |res, bonus|
#   #       if game.players.map { |player| player.player.send bonus[:type] }.uniq.include?(bonus[:how])
#   #         return res + bonus[:count]
#   #       end
#   #       res
#   #     end
#   #   end
#   #
#   #   private
#   #
#   #   def recalculate_monster_attrs!
#   #     @power = @level + bonus_power
#   #     # @treasures_count
#   #   end
#   #
#   # end
#   #
#   # class BattlePlayer < BattleCharacter
#   #
#   #   attr_reader :id, :role, :player
#   #   attr_accessor :treasures_count
#   #
#   #   def initialize(player, role = :main)
#   #     @id = object_id
#   #     @role = role
#   #     @attached_cards = []
#   #     player.change_state ::Manchkin::PlayerInBattle.instance
#   #     # @klass = player.class
#   #     @player = player
#   #     @power = @player.power
#   #     @treasures_count = 0
#   #
#   #     recalculate_player_power!
#   #   end
#   #
#   #   def attach_card(card)
#   #     @attached_cards.push card
#   #
#   #     recalculate_player_power!
#   #   end
#   #
#   #   private
#   #
#   #   def recalculate_player_power!
#   #     @power = @player.power
#   #     # + attached card
#   #   end
#   #
#   # end
#   #
#   # class Battle
#   #
#   #   attr_reader :players, :monsters, :state, :id
#   #   attr_accessor :winner
#   #
#   #   def initialize(player, card_monster, logger)
#   #     # @game = game
#   #     @id = object_id
#   #     @players = []
#   #     @players.push ::Manchkin::BattlePlayer.new player
#   #     @monsters = []
#   #     @monsters.push ::Manchkin::BattleMonster.new card_monster, self
#   #
#   #     @winner = nil
#   #     @state = ::Manchkin::BattleInProgress.instance
#   #     @logger = logger
#   #
#   #     # recalculate_monsters_bonuses!
#   #
#   #     logger.push "начался бой между #{player.name} и <#{card_monster.id}>"
#   #     logger.action :battle, owner: battle_owner, monsters: @monsters
#   #   end
#   #
#   #   def change_state(state)
#   #     @state = state
#   #   end
#   #
#   #   # battle actions----------------------------------------
#   #
#   #   def try_to_leave_monster(player, monster_card, number)
#   #     p 'battle:ttm'
#   #     p can_perform_action? __method__
#   #     if can_perform_action? __method__
#   #       @state.try_to_leave_monster self, player, monster_card, number
#   #     end
#   #   end
#   #
#   #   def end_battle
#   #     if can_perform_action? __method__
#   #       @state.end_battle self
#   #     end
#   #   end
#   #
#   #   def add_monster(card_monster)
#   #     if can_perform_action? __method__
#   #       @state.add_monster self, card_monster
#   #     end
#   #   end
#   #
#   #   def add_helper(player)
#   #     if can_perform_action? __method__
#   #       @state.add_monster self, player
#   #     end
#   #   end
#   #
#   #   # //battle actions----------------------------------------
#   #
#   #
#   #   def current_winner # :manchkin or :monster
#   #     return :manchkin
#   #     players_power = @players.inject(0) { |sum, item| sum + item.power }
#   #     monsters_power = @monsters.inject(0) { |sum, monster| sum + monster.power unless monster.defeated? }
#   #     p 'monsters_power ' + monsters_power.to_s
#   #     # monsters_absolute_power = monsters_power + monsters_bonus
#   #     # p 'bonus! ' + monsters_bonus.to_s
#   #
#   #     return :manchkin if players_power > monsters_power
#   #     :monster
#   #   end
#   #
#   #   # def monsters_bonus
#   #   #   @monsters.inject(0) do |res, monster|
#   #   #     monster.bonus_power(@players) + res
#   #   #   end
#   #   # end
#   #
#   #   def battle_owner
#   #     @players.find { |pl| pl.role == :main }
#   #   end
#   #
#   #   def battle_helper
#   #     @players.find { |pl| pl.role == :helper }
#   #   end
#   #
#   #   def get_monster_by_monster_card(monster_card)
#   #     @monsters.find { |monster_char| monster_char.monster == monster_card }
#   #   end
#   #
#   #   def get_player_char_by_player(player)
#   #     @players.find { |player_char| player_char.player == player }
#   #   end
#   #
#   #   private
#   #
#   #   # def recalculate_monsters_bonuses!
#   #   #   @monsters.each do |monster|
#   #   #     monster.bonus_power = monster.bonuses.inject(0) do |res, bonus|
#   #   #       if @players.map { |player| player.player.send bonus[:type] }.uniq.include?(bonus[:how])
#   #   #         return res + bonus[:count]
#   #   #       end
#   #   #       res
#   #   #     end
#   #   #   end
#   #   # end
#   #
#   #   def can_perform_action?(method)
#   #     @state.class.instance_methods(false).include? method
#   #   end

#   # end
# end