require 'singleton'

module Manchkin2
  class GameState
    include Singleton

    # def logger(game, message, players)
    #   game.logger.push
    # end

  end

  class GameNew < GameState
    def add_player(game, uniq_name, sex)
      game.players.push Manchkin2::Player.new uniq_name, sex
    end

    def start_game(game)
      game.add_player 'sol', :male

      game.deck.shuffle!

      game.players.each do |p|
        p.send :add_cards, game.deck.draw_start_hand
        p.send :attach_to_game, game
      end

      game.instance_variable_set "@current_player", game.players.first

      game.current_player.send :change_state, ::Manchkin2::PlayerOpenDoor.instance
      game.send :change_state, ::Manchkin2::GameInProgress.instance
    end
  end

  class GameInProgress < GameState

    def throw_dice(game)
      3
    end

    def open_door(game)
      logger = game.logger
      card = game.deck.draw_card

      game.other_players.each { |pl| logger.push "#{game.current_player.name} вытянул <#{card.id}>", pl.id }
      logger.push "тебе досталось <#{card.id}>", game.current_player.id

      p 'game open door from state'

    end
  end
end