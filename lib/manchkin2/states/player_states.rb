require 'singleton'

module Manchkin2
  class PlayerState
    include Singleton

    def method_missing(m, *args, &block)
      p 'method missing'
    #   p m
    #   p args
    #   p block
    #   args.first.game.throw_dice
    #   # если сюда попал, значит это ghost метод, уже проверил что он есть в гост методах игрока
    end

    # def send_exchange_request(initiator, receiver)
    #   initiator.state_before_exchange_dialog = initiator.state
    #   initiator.change_state ::Manchkin2::PlayerOfferDialog.instance
    #
    #   receiver.state_before_exchange_dialog = receiver.state
    #   receiver.change_state ::Manchkin2::PlayerIncomingDialog.instance
    # end

    def throw_dice(player, args = {})
      player.game.throw_dice
    end

  end

  class PlayerDoRequiredAction < PlayerState

  end

  # class PlayerChooseCard < PlayerState
  #
  # end

  class PlayerWaitingConfirmation < PlayerState # for throw dice or apply curse

    # def throw_dice(player)
    #   super player
    # end

    def play_wish_ring(player) # delete this after testing
      player.instance_variable_set "@action_result", 3
    end

    # def set_action_result(player, action_result)
    #   player.instance_variable_set "@action_result", action_result
    # end
    # + actions, which can change result of confirmable action
    def confirm(player)
      play_wish_ring player
      # сменить стейт напрямую, без коллбеков
      player.instance_variable_set "@state", player.instance_variable_get("@state_need_to_return_after_transition_from_wait_confirm")

      if player.instance_variable_get("@action_when_confirm").respond_to? :call
        player.instance_variable_get("@action_when_confirm").call player
      end

      player.instance_variable_get("@after_actions").call player.instance_variable_get("@action_result")

      player.instance_variable_set "@state_need_to_return_after_transition_from_wait_confirm", nil
      player.instance_variable_set "@action_result", nil
      player.instance_variable_set "@after_actions", nil
      player.instance_variable_set "@action_when_confirm", nil

    end
  end

  class PlayerReady < PlayerState

  end

  class PlayerOpenDoor < PlayerState

    # def send_exchange_request(initiator, receiver)
    #   super initiator, receiver
    # end

    def open_door(player)
      p 'open_door'
      player.game.send :open_door
    end
  end

  class PlayerOpenedDoor < PlayerState # если открыл дверь в открытую и не встретил монстра
    def look_for_trouble(player, monster_card)
      p 'look_for_trouble'
    end

    def search_room(player) #
      p 'search_room'
      player.game.search_room player
    end
  end

  class PlayerInBattle < PlayerState
    def end_battle(player)
      player.game.end_battle
    end

  end

  class PlayerWinBattle < PlayerState
    def level_up_after_win(player) # стандартное поведение после победы над монстром
      # player.level_up!
    end
  end

  class PlayerTryToLeave < PlayerState
    def try_to_leave_monster(player, monster_card)
      # player.game.try_to_leave_monster(player, monster_card)
    end
  end

  class PlayerLeftMonster < PlayerState

  end

  class PlayerSufferBadStuff < PlayerState

  end

  class PlayerEndingTurn < PlayerState

  end

  class PlayerDead < PlayerState

  end

  class PlayerOfferDialog < PlayerState
    def terminate_offer(player)
      player.exchange_dialog.terminate
    end
  end

  class PlayerIncomingDialog < PlayerState
    def accept_dialog(player)
      player.exchange_dialog.accept_dialog
    end

    def decline_dialog(player)
      player.exchange_dialog.terminate
    end
  end

  class PlayerExchangeDialog < PlayerState
    def add_card(player, card)
      player.exchange_dialog.add_card player, card
    end

    def remove_card(player, card)
      player.exchange_dialog.remove_card player, card
    end
  end

end