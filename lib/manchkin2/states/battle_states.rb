require 'singleton'

module Manchkin2
  class BattleState
    include Singleton

    def character(battle, char_id)
      (battle.players + battle.monsters).find { |char| char.id == char_id }
    end

  end

  class BattleInProgress < BattleState

    def add_character(battle, args = {})
      if args.has_key? :monster
        battle.send :add_monster, args[:monster]
      else
        if args.has_key?(:player) && args.has_key?(:player_role)
          battle.send :add_player, args[:player], args[:player_role]
        end
      end
    end
    # def start(battle)
    #   puts battle
    #   puts 'wowoww :)'
    #   6
    # end
    #
    # def end_battle(battle)
    #   battle.winner = battle.current_winner
    #   if battle.winner == :manchkin
    #     battle.change_state ::Manchkin::BattlePlayersWin.instance
    #     battle.players.each { |character| character.player.change_state Manchkin::PlayerWinBattle.instance }
    #
    #     battle.battle_owner.treasures_count = battle.monsters.inject(0) { |sum, item| sum + item.treasures_count }
    #     battle.battle_owner.player.level_up_after_win
    #   else
    #     battle.change_state ::Manchkin::BattlePlayersTryLeave.instance
    #     battle.players.each { |character| character.player.change_state Manchkin::PlayerTryToLeave.instance }
    #   end
    # end
    #
    # def add_monster(battle, card_monster)
    #   battle.monsters.push Manchkin::BattleMonster.new card_monster, battle
    # end
    #
    # def add_helper(battle, player)
    #   battle.players.push Manchkin::BattlePlayer.new player, :helper
    # end

  end

  class BattlePlayersWin < BattleState
    # def give_treasures_to_helper
    #
    # end
  end


  class BattlePlayersTryLeave < BattleState
    # def try_to_leave_monster(battle, player, monster_card, number)
    #   p 'throw on ' + number.to_s
    #
    #   monster_char = battle.get_monster_by_monster_card monster_card
    #   unless monster_char.defeated?
    #     if (5..6).include? number
    #     # if (1..6).include? number
    #       # не убёг))
    #       player.change_state ::Manchkin::PlayerSufferBadStuff.instance
    #       monster_char.monster.bad_stuff.call player
    #       # надо к игроку применить непотребство монстра
    #       player.change_state ::Manchkin::PlayerEndingTurn.instance
    #     else
    #       p 'убег'
    #       player.change_state ::Manchkin::PlayerLeftMonster.instance
    #       # убежал, но надо проверить мож монстр както навредил
    #     end
    #   end
    #   # чтото надо с battle сделать после всего этого
    # end
  end

end