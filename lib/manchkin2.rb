require "manchkin2/version"
require "manchkin2/support/id_generator"

require "manchkin2/card"
require "manchkin2/deck"
require "manchkin2/game"
require "manchkin2/player"
require "manchkin2/battle"
require "manchkin2/exchange_dialog"
require "manchkin2/logger"

module Manchkin2

  class NonSupportedExtension < StandardError
    def initialize(version)
      super "Extension #{version.to_s} not supported"
    end
  end

  class DefinitionProxy

    attr_reader :cards

    def initialize
      @cards = []
    end

    def card(card_class, &block)
      card_factory = CardFactory.new
      if block_given?
        card_factory.instance_eval &block
      end
      card_instance = Object.const_get("::Manchkin2::#{card_class.capitalize}").allocate

      card_factory.attributes.each do |attr_name, attr_val|
        card_instance.instance_variable_set :"@#{attr_name}", attr_val
        card_instance.class.class_eval { send :attr_reader, :"#{attr_name}" }
      end

      card_instance.instance_variable_set :@card_type, card_instance.card_type
      card_instance.instance_variable_set :@subtype, card_instance.subtype
      card_instance.instance_variable_set :@id, card_instance.object_id
      # card_instance.class.class_eval { send :attr_reader, :card_type, :subtype }
      # card_instance.class.class_eval { send :attr_reader, :subtype }


      @cards.push card_instance
    end

  end

  class CardFactory < BasicObject
    def initialize
      @attributes = {}
    end

    attr_reader :attributes

    def method_missing(name, *args, &block)
      attributes[name] = args[0]
    end
  end




  SUPPORT_EXTENSIONS = [:classic, :unnatural_axe, :clerical_errors]

  class << self
    attr_accessor :loaded_cards
  end

  def self.new_game(version = :classic)
    load_cards version
    deck = Deck.new @loaded_cards
    Manchkin2::Game.new deck
  end

  @tmp_cards = []

  def self.define_deck(deck_name, previous_extension = nil, &block)
    definition_proxy = DefinitionProxy.new
    definition_proxy.instance_eval(&block)

    unless previous_extension.nil?
      @tmp_cards += definition_proxy.cards
      require "manchkin2/decks/#{previous_extension.to_s}"
    else
      Manchkin2.loaded_cards = @tmp_cards + definition_proxy.cards
    end
  end

  private

  def self.load_cards(version)
    if support_extensions.include? version
      require "manchkin2/decks/#{version.to_s}"
    else
      raise NonSupportedExtension, version
    end
  end

  def self.support_extensions
    SUPPORT_EXTENSIONS
  end

end
